#!/bin/bash
#update canvas with SMS and CV graphs every hour
#source drawmonitorgraphs smsfilename cvfilename
#provide only the names, the EOS paths are already written in the C code
while true;
 do
  root -l -q /home/sndmonitoring/DataQualityMonitoring/detectorcontrolsystem/cplusplus_scripts/drawmonitorgraphs.C\(\"$1\",\"$2\"\)
  echo "Updated graph file"
  sleep 3600
 done
