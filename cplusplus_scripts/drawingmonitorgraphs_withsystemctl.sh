#!/bin/bash

source /cvmfs/sndlhc.cern.ch/SNDLHC-2022/July14/setUp.sh
export ALIBUILD_WORK_DIR=$HOME/SNDBuild/sw/
unset MODULES_RUN_QUARANTINE
eval `alienv load sndsw/latest-master-release`
source $HOME/DataQualityMonitoring/detectorcontrolsystem/configpydim.sh


LATESTMQTTFILE=$(ls -rt /eos/experiment/sndlhc/emulsionData/dcs_monitoring/ | tail -n 1)
LATESTCVFILE=$(ls -rt /eos/experiment/sndlhc/emulsionData/cv_monitoring/ | tail -n 1)

echo "Drawing data from "$LATESTMQTTFILE "and "$LATESTCVFILE

source $HOME/DataQualityMonitoring/detectorcontrolsystem/cplusplus_scripts/startdrawingmonitorgraphs.sh $LATESTMQTTFILE $LATESTCVFILE
