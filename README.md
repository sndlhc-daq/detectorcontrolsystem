# Detector Control System software interfacing Safety System Monitoring

First version of repository for DCS software, only a few classes currently available

Requires ROOT 6.26 

## Get the code
* Clone the git repo: `ssh://git@gitlab.cern.ch:7999/sndlhc-daq/detectorcontrolsystem.git` or `https://gitlab.cern.ch/sndlhc-daq/detectorcontrolsystem.git`.
* Enter the repo and fetch the submodules by doing:
```
git submodule init
git submodule update

## Build instructions

Build is performed with CMake

cd path/to/detectorcontrolsystem
mkdir build
cd build
cmake ..
make -j8
