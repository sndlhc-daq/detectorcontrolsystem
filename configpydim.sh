#!/bin/sh
export DIMDIR=/home/sndmonitoring/dimCore/ #replace with the path of your dimCore installation!
export PYTHONPATH=$PYTHONPATH:/usr/local/lib/python3.6/site-packages/
export LD_LIBRARY_PATH=$DIMDIR/linux:$LD_LIBRARY_PATH #so pydim can find libdim.so
export DIM_DNS_NODE=dipnsgpn1.cern.ch #server with CV data
export DIM_DNS_PORT=2506 #port to access server above
