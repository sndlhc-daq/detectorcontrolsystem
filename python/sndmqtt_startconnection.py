import paho.mqtt.client as mqtt

import ROOT as r
#for now, compiled libraries cannot be loaded (must be shared libraries?) just loading c++ files
r.gROOT.ProcessLine(".include /home/sndmonitoring/DataQualityMonitoring/detectorcontrolsystem/external/json/single_include/nlohmann")
r.gROOT.ProcessLine(".L /home/sndmonitoring/DataQualityMonitoring/detectorcontrolsystem/lib/coldbox_parser.cpp")
r.gROOT.ProcessLine(".L /home/sndmonitoring/DataQualityMonitoring/detectorcontrolsystem/lib/dcs_presenter.cpp")

#initialization
mainpresenter = r.DCSPresenter()
mainpresenter.SetOutputFile("/eos/experiment/sndlhc/emulsionData/dcs_monitoring/dcs_output")

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("monitor")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    npoints_update = 100 #about every 30 min, update ttree
    #a = msg.topic+" "+str(msg.payload)
    strmessage = msg.payload
    strmessage = strmessage.replace(b'nan', bytes("-1000", encoding='utf8')) #cannot parse nan, but at least they will be stored
    print(strmessage)
    mainpresenter.NextMonitoringString(strmessage)
    mainpresenter.FillTree()
    if (mainpresenter.GetSMSTree().GetEntries() % npoints_update == 0):
     print("Update TTree")
     mainpresenter.SaveTree()

client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("snd-vm-dcs-1.cern.ch", 1883, 60)

# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
client.loop_forever()
