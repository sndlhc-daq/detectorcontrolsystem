#!/bin/bash

source /cvmfs/sndlhc.cern.ch/SNDLHC-2022/July14/setUp.sh
export ALIBUILD_WORK_DIR=$HOME/SNDBuild/sw/
unset MODULES_RUN_QUARANTINE
eval `alienv load sndsw/latest-master-release`
source $HOME/DataQualityMonitoring/detectorcontrolsystem/configpydim.sh
python $HOME/DataQualityMonitoring/detectorcontrolsystem/python/CVtemphumidity_DIM_SND_readout.py
