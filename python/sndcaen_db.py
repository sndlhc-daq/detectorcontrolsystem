#!/usr/bin/env python3

import os.path as op
from influxdb import InfluxDBClient
from SndCaenManager import SndCaenManager
from datetime import datetime
from time import sleep
from snd_influx_helper.helpers.caen_helper import SndCaenInfluxHelper

def main():
    # manager = SndCaenManager('conf_SND.toml')
    dbHelper = SndCaenInfluxHelper()
    manager = SndCaenManager(op.join(op.dirname(op.abspath(__file__)), 'config_SND.toml'))
    lvFields = ('VMon', 'IMon', 'VCon', 'Temp', 'Pw', 'Status')
    hvFields = ('VMon', 'IMon', 'Pw', 'Status')

    hvData = manager.getChannelInfo('bias', None)
    lvData = manager.getChannelInfo('board', None)
    timestamp = datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ')

    dbHelper.addLvPoints(lvData, timestamp)
    dbHelper.addHvPoints(hvData, timestamp)
    dbHelper.writePoints()

    
    # if counter >= 1:
    #     dbClient = InfluxDBClient(host='dbod-snd-influx-test.cern.ch', port=8088, username='sms', password='smspass', ssl=True)
    #     print(datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%SZ'))
    #     dbClient.switch_database('monitoring-system')
    #     dbClient.write_points(points)
    #     points.clear()


if __name__ == '__main__':
    main()
