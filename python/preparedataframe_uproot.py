import uproot
import numpy as np
import os
import pandas as pd
inputpath = '/eos/experiment/sndlhc/emulsionData/dcs_monitoring/'
def ttreeintodf(inputfilename,mypath):
 '''converts ttree into pandas dataframe with uproot'''
 inputfile = uproot.open(mypath+inputfilename)
 inputtree = inputfile['smstree']
 tarr = inputtree.arrays()
 
 df = pd.DataFrame({'monitortime':tarr['monitortime'],'coolingon':tarr['coolingon'],\
    'temp1':tarr["temp"][:,0],'temp2':tarr["temp"][:,1],'temp3':tarr["temp"][:,2],'temp4':tarr["temp"][:,3],'temp5':tarr["temp"][:,4],\
    'relhum1':tarr["relhum"][:,0],'relhum2':tarr["relhum"][:,1],'relhum3':tarr["relhum"][:,2],'relhum4':tarr["relhum"][:,3],'relhum5':tarr["relhum"][:,4],\
    'smk1':tarr["smk"][:,0],'smk2':tarr["smk"][:,1],'smk3':tarr["smk"][:,2],})
 return df

#empty dataframe, to be filled with all data
totaldf = pd.DataFrame()
#get inputfiles from directory
inputfiles = os.listdir(inputpath)
for filename in inputfiles:
 if (filename[:10]=="dcs_output"): #these are the files we are looking for
  try:
    totaldf = pd.concat([totaldf,ttreeintodf(filename,inputpath)])
  except:
    print("Error in reading file ",filename," probably currently file")

#sorting by time
totaldf = totaldf.sort_values("monitortime") 
#now reducing it is easy in pandas
totaldf = totaldf.reset_index(drop=True)
nhour = (totaldf["monitortime"]/3600.).astype(int) #number of hours from EPOCH
totaldf["nhour"]=nhour


readabledate = pd.to_datetime(totaldf["monitortime"],unit='s')

def store_reduceddf():
 reduceddf = totaldf.groupby("nhour").mean() #average them together
 reduceddf = reduceddf.astype({'monitortime':'uint64'}) #I want the average to be still an int
 reduceddf.to_csv("hourly_summary.csv",index=False)
