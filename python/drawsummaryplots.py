'''plotting DCS monitor summary (13 June 2022)'''
import pandas as pd
import matplotlib.pyplot as plt

df = pd.read_csv("hourly_summary.csv")
#converting timestamp into readable format 
df["timeaxis"]=pd.to_datetime(df["monitortime"],unit='s')
#figure 1, temperatures
plt.figure()
plt.plot(df["timeaxis"],df["temp1"],"r",label="temperature sensor 1")
plt.plot(df["timeaxis"],df["temp2"],"y",label="temperature sensor 2")
plt.plot(df["timeaxis"],df["temp3"],"b",label="temperature sensor 3")
plt.plot(df["timeaxis"],df["temp4"],"m",label="temperature sensor 4")
plt.plot(df["timeaxis"],df["temp5"],"k",label="temperature sensor 5")
plt.legend()
#figure 1, humidities
plt.figure()
plt.plot(df["timeaxis"],df["relhum1"],"r",label="relative humidity sensor 1")
plt.plot(df["timeaxis"],df["relhum2"],"y",label="relative humidity sensor 2")
plt.plot(df["timeaxis"],df["relhum3"],"b",label="relative humidity sensor 3")
plt.plot(df["timeaxis"],df["relhum4"],"m",label="relative humidity sensor 4")
plt.plot(df["timeaxis"],df["relhum5"],"k",label="relative humidity sensor 5")
plt.legend()

plt.show()