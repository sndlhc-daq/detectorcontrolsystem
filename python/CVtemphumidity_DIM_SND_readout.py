#!/usr/bin/env python
# -*- coding: UTF-8 -*-
"""
A.Iuliano

Accessing information from CV Sensor, published via DIP

Before launching this code, do in tcsh (NOT bash)

cd yourdimcorefolder
source .setup
setenv DIM_DNS_NODE dipnsgpn1.cern.ch
setenv DIM_DNS_PORT 2506


"""

import ROOT as r
import sys
import time
sys.path.append("/usr/local/lib64/python3.6/site-packages/pydim3-3.0.4-py3.6-linux-x86_64.egg") #temporary fix, adding the folder to PythonPath does not work
# Import the pydim module
import pydim
import struct

#preparing the presenter
r.gROOT.ProcessLine(".L /home/sndmonitoring/DataQualityMonitoring/detectorcontrolsystem/lib/cv_presenter.cpp")
mainpresenter = r.CVPresenter()
mainpresenter.SetOutputFile("/eos/experiment/sndlhc/emulsionData/cv_monitoring/cvdatafile")

def client_callback1(temp, test1):
    """
    Callback function for the service 1.

    Callback functions receive as many arguments as values are returned by the
    service. For example, as the service 1 returns only one string this callback
    function has only one argument.

    """
    if isinstance(temp,str):
     temp = struct.unpack('f',bytes(temp,'utf-8'))
    else:
     temp = struct.unpack('f',temp)
    print("Client callback function for service 1")
    print("Message received: {}".format(temp[0]))

    mainpresenter.FillTree(temp[0],True) #true means fill temperature

def client_callback2(hum, test1):
    """
    Callback function for service 2.

    As the service 1 returned one argument too here
    """
    if isinstance(hum,str):
     hum = struct.unpack('f',bytes(hum,'utf-8'))
    else:
     hum = struct.unpack('f',hum)
    print("Client callback function for service 2")
    print("Values received: {}".format(hum[0]))

    mainpresenter.FillTree(hum[0],False) #false means fill humidity

def main():
    """
    A client for subscribing to two DIM services
    """

    # Again, check if a Dim DNS node has been configured.
    # Normally this is done by setting an environment variable named DIM_DNS_NODE
    # with the host name, e.g. 'localhost'.
    #
    if not pydim.dis_get_dns_node():
        print("No Dim DNS node found. Please set the environment variable DIM_DNS_NODE")
        sys.exit(1)


    # The function `dic_info_service` allows to subscribe to a service.
    # The arguments are the following:
    # 1. The name of the service.
    # 2. Service description string
    # 3. Callback function that will be called when the service is
    #    updated.
    '''res1 = pydim.dic_info_service("example-service-1", "C", client_callback1)
    res2 = pydim.dic_info_service("example-service-2", "D:1;I:1;", client_callback2)'''

    res1 = pydim.dic_info_service("dip/CV/EAU/DEMI/L01/SND/FCUL-00053/FCUL00053_TT5101/SND_TT5101_T_BOITE_FROIDE/DipData", client_callback1)
    res2 = pydim.dic_info_service("dip/CV/EAU/DEMI/L01/SND/FCUL-00053/FCUL00053_RH5101/SND_RH_5101_HUM_BOITE_FROIDE/DipData", client_callback2)

    if not res1 or not res2:
        print("There was an error registering the clients")
        sys.exit(1)

    # Wait for updates
    nsecondstosave = 900 #saving every tot seconds
    while True:
        mainpresenter.SaveTree()
        #wating tot seconds before saving again
        time.sleep(nsecondstosave)

    
if __name__=="__main__":
    main()
