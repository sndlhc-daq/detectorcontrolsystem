#!/bin/bash

source $HOME/.venv/py38-mon/bin/activate
export DIMDIR=/home/sndmonitoring/dimCore/ 
export LD_LIBRARY_PATH=$DIMDIR/linux:$LD_LIBRARY_PATH # so pydim can find libdim.so
export DIM_DNS_NODE=dipnsgpn1.cern.ch,dipnsgpn2.cern.ch # server with CV data
export DIM_DNS_PORT=2506 # port to access server above

python $HOME/DataQualityMonitoring/detectorcontrolsystem/python/sndencv_db.py