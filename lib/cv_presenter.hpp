#ifndef CVPRESENTER
#define CVPRESENTER
//CVPresenter: stores two TTrees with information from EN-CV sensor, parsed with pydim
#include "TObject.h"
#include "TTree.h"
#include "TFile.h"
#include "TTimeStamp.h"
#include <string>
#include <iostream>

class CVPresenter : public TObject
{
    public:
     //constructor
     CVPresenter();
     //destructor
     ~CVPresenter();
     //accessors
     TTree* GetTempTree() const {return cvtemp;}
     TTree* GetHumTree() const {return cvhum;}
     //modifiers
     void ClearTree(); //reset the tree entries, preserve the structure
     void FillTree(Double_t value, Bool_t istemperature); //temptree true for temperature, false for humidity
     void SetOutputFile(const char* outputfilename);
     void SaveTree() const;


    private:
     TFile * outputfile; //pointing to outputfile
     TTree * cvtemp;
     TTree * cvhum; //TTree of monitor sensors
     Double_t hum, temp;

     TTimeStamp timestamp;
     ULong64_t timenow; //Time flies by when contemplating life, the universe, and everything...

     ClassDef(CVPresenter,1);
};

#endif /* CVPRESENTER */
