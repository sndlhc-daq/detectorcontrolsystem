#ifndef COLDBOXPARSER
#define COLDBOXPARSER
#include <string>
#include <iostream>
#include <fstream>
#include <sstream>
#include "json.hpp"

using json = nlohmann::json;

class ColdBoxParser{
//Class to parse json monitoring messages from coldbox.
 public:
    ColdBoxParser(){
        //initialization values;
        isalert = false;
    }
    //accessors
    double GetTemperature(int sensor) const;
    double GetHumidity(int sensor) const;
    bool IsCoolingOn() const;
    bool IsSmokingTriggered(int sensor) const;
    std::string GetAlertLevel() const;
    json GetStatus() const{return cb_status;}
    json GetSetParameters() const{return cb_setparameters;}
    void PrintParList() const;

    //modifiers
    void UpdateStatus(std::string status,bool alarmsignal = false);
    void UpdateStatusFromFile (const char* inputfilename, bool alarmsignal = false);
    void UpdateParameters(std::string status);
    void UpdateParametersFromFile (const char* inputfilename);
    //adding/setting a parameter to the setparameter string
    void SetParameter(int ipar, double value);
    void SetIntParameter(int ipar, int value);
    void ExcludeTemperatureSensor(int isensor);
    void ExcludeSmokingSensor(int isensor);
    void ActivateAllSensors();
    void ClearParameters() {cb_setparameters.clear();}

 private:
    json cb_status;
    json cb_setparameters;
    bool isalert;
    //names of parameters that can be set via SetParameter (excluded_sensors list is filled by dedicated function)
    std::string parnames[9] = {
        "alarm_min_n_t_rh",
        "alarm_min_n_smk",
        "alarm_t_thr_max",
        "alarm_rh_thr_max",
        "alarm_t_thr_min",
        "alarm_rh_thr_min",
        "monitor_update_time",
        "alarm_update_time",
        "cooling_grace_time"
    };


};

#endif /* COLDBOXPARSER */
