#include "coldbox_parser.hpp"

void ColdBoxParser::UpdateStatus(std::string status,bool alarmsignal){
    //update status of the coldbox with new monitoring information
    cb_status = json::parse(status);
    isalert = alarmsignal;
}

void ColdBoxParser::UpdateStatusFromFile (const char* inputfilename, bool alarmsignal){
    //update status of the coldbox with new monitoring information stored in a .json file
    std::ifstream myFile(inputfilename);
    std::ostringstream tmp;
    tmp << myFile.rdbuf();
    std::string s = tmp.str();
    //string ready, now we can update status as normal
    UpdateStatus(s,alarmsignal);

}

void ColdBoxParser::UpdateParameters (std::string parameters){
    //update parameters to be sent to coldbox from a string
    cb_setparameters = json::parse(parameters);

}

void ColdBoxParser::UpdateParametersFromFile (const char* inputfilename){
    //update parameters to be sent to coldbox from a .json file
    std::ifstream myFile(inputfilename);
    std::ostringstream tmp;
    tmp << myFile.rdbuf();
    std::string s = tmp.str();
    //string ready, now we can update parameters as normal
    UpdateParameters(s);
}

double ColdBoxParser::GetTemperature(int sensor) const{
    //returns temperature from sensor
    std::string alertstring = "";
    if (isalert) alertstring = alertstring + "/readings";//need to add readings prefix for alerts
    
    //need to prepare a char* for sprintf, then convert it to string
    const int pathcharsize = 50; //more than required letters
    char tpath[pathcharsize];
    std::sprintf(tpath,"/environment/t_rh-%i/t",sensor);
    std::string temperaturepath = (std::string) tpath; //from char* to string 

    json::json_pointer pointer_temperature(alertstring+temperaturepath);
    double temperature = cb_status.at(pointer_temperature);
    return temperature;
}

double ColdBoxParser::GetHumidity(int sensor) const{
    //returns temperature from sensor
    std::string alertstring = "";
    if (isalert) alertstring = alertstring + "/readings";//need to add readings prefix for alerts

    //need to prepare a char* for sprintf, then convert it to string
    const int pathcharsize = 50; //more than required letters
    char hpath[pathcharsize];
    std::sprintf(hpath,"/environment/t_rh-%i/rh",sensor);
    std::string humiditypath = (std::string) hpath; //from char* to string 

    //returns humidity from sensor
    json::json_pointer pointer_humidity(alertstring+humiditypath);
    double humidity = cb_status.at(pointer_humidity);
    return humidity;
}

bool ColdBoxParser::IsCoolingOn() const{
    //returns temperature from sensor
    std::string alertstring = "";
    if (isalert) alertstring = alertstring + "/readings";//need to add readings prefix for alerts
    //check if cooling is on
    json::json_pointer pointer_cooling(alertstring+"/cooling/status");
    int coolingstatus = cb_status.at(pointer_cooling);
    return (bool) coolingstatus;
}

bool ColdBoxParser::IsSmokingTriggered(int sensor) const{
    //returns temperature from sensor
    std::string alertstring = "";
    if (isalert) alertstring = alertstring + "/readings";//need to add readings prefix for alerts

    //need to prepare a char* for sprintf, then convert it to string
    const int pathcharsize = 50; //more than required letters
    char spath[pathcharsize];
    std::sprintf(spath,"/smoke/smk-%i/triggered",sensor);
    std::string smokepath = (std::string) spath; //from char* to string 

    //check if smoking sensor has been triggered
    json::json_pointer pointer_smoking(alertstring+smokepath);
    int smokingstatus = cb_status.at(pointer_smoking);
    return (bool) smokingstatus;
}

std::string ColdBoxParser::GetAlertLevel() const{
    //check level of alert. For monitoring messages, just return "monitoring"
    if (!isalert) return "monitoring";
    json::json_pointer pointer_alert("/level");
    //returning alert level
    std::string alertlevel = cb_status.at(pointer_alert);
    return alertlevel;


}

void ColdBoxParser::SetParameter(int ipar, double value){
    //setting paramaters to value. 
    //JSON numbers do not distinguish between int and double
    cb_setparameters["set_parameters"][parnames[ipar]] = value;
}

void ColdBoxParser::SetIntParameter(int ipar, int value){
    //Setting parameter ipar to value. Here, value is expected to be integer
    cb_setparameters["set_parameters"][parnames[ipar]] = value;
}

void ColdBoxParser::ExcludeSmokingSensor(int isensor){
    //need to prepare a char* for sprintf, then convert it to string
    const int pathcharsize = 50; //more than required letters
    char spath[pathcharsize];
    std::sprintf(spath,"smk-%i",isensor);
    std::string smokepath = (std::string) spath; //from char* to string 
    if (!cb_setparameters.contains("excluded_sensors"))
      cb_setparameters["excluded_sensors"] = {smokepath};
    else
      cb_setparameters["excluded_sensors"].insert(cb_setparameters["excluded_sensors"].end()-1,smokepath);
}

void ColdBoxParser::ExcludeTemperatureSensor(int isensor){
    //need to prepare a char* for sprintf, then convert it to string
    const int pathcharsize = 50; //more than required letters
    char tpath[pathcharsize];
    std::sprintf(tpath,"t_rh-%i",isensor);
    std::string temperaturepath = (std::string) tpath; //from char* to string 
    if (!cb_setparameters.contains("excluded_sensors"))
     cb_setparameters["excluded_sensors"] = {temperaturepath};
    else
     cb_setparameters["excluded_sensors"].insert(cb_setparameters["excluded_sensors"].end()-1,temperaturepath);
}

void ColdBoxParser::ActivateAllSensors(){
    //excluded_sensors is set to an empty list. Required to reactivate excluded sensors
    cb_setparameters["excluded_sensors"] = {};
}

void ColdBoxParser::PrintParList() const{
    //dumping list of parameters, with the number to call them
    std::cout<<"Printing list of parameters"<<std::endl;
    for (int ipar=0; ipar < 9; ipar++){
        std::cout<<ipar<<"\t"<<parnames[ipar]<<std::endl;
    }
    std::cout<<"Use SetParameter(ipar,value) to set these parameters"<<std::endl;
    std::cout<<"Use ExcludeTemperatureSensor(isensor) and ExcludeSmokingSensor(isensor) to exclude a sensor"<<std::endl;
}