#ifndef DCSPRESENTER
#define DCSPRESENTER
//DCSPresenter: stores a TTree from SMS information, parsed via ColdBoxParser
#include "coldbox_parser.hpp"
#include "TObject.h"
#include "TTree.h"
#include "TFile.h"
#include "TTimeStamp.h"
#include <string>
#include <iostream>

class DCSPresenter : public TObject
{
    public:
     //constructor
     DCSPresenter();
     //destructor
     ~DCSPresenter();
     //accessors
     TTree* GetSMSTree() const {return smstree;}
     ColdBoxParser GetCBParser() const {return cbparser;}
     //modifiers
     void ClearTree(); //reset the tree entries, preserve the structure
     void NextMonitoringString(std::string nextstring); //receive the string from the MQTT broker, pass it to Parser;
     void NextMonitoringFile(const char* inputfilename); //receive the string from the MQTT broker, pass it to Parser;
     void FillTree(); //fill graphs with the current parser value
     void SetOutputFile(const char* outputfilename);
     void SaveTree() const;


    private:
     ColdBoxParser cbparser;
     const int static ntempsensors = 5; //number of temperature/humidity sensors
     const int static nsmksensors = 3; //number of smoke sensors
     TFile * outputfile; //pointing to outputfile
     TTree * smstree; //TTree of monitor sensors
     Double_t relhum[5], temp[5];
     Bool_t smk[3];
     Bool_t coolingon;

     TTimeStamp timestamp;
     ULong64_t timenow; //Time flies by when contemplating life, the universe, and everything...

     ClassDef(DCSPresenter,2);
};

#endif /* DCSPRESENTER */
