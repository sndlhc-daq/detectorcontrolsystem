#include "cv_presenter.hpp"

CVPresenter::CVPresenter(){
    timestamp = TTimeStamp();
    //constructor, TTree initialization. Temperature and humidity come from two different sources, they get into two trees
    cvtemp = new TTree("cvtemp","TTree with temperature data from cv sensor");
    cvtemp->Branch("monitortime",&timenow, "monitortime/l"); //l is for long unsigned int branches
    cvtemp->Branch("temp",&temp,"temp/D");
    cvhum = new TTree("cvhum","TTree with temperature data from cv sensor");
    cvhum->Branch("monitortime",&timenow, "monitortime/l"); //l is for long unsigned int branches
    cvhum->Branch("hum",&hum,"hum/D");
}

CVPresenter::~CVPresenter(){
    if (outputfile){ 
        outputfile->Close(); //close eventually still opened file
    }
}

void CVPresenter::ClearTree(){
 //clearing all TTree entries, but preserves structures
 cvtemp->Reset();
 cvhum->Reset();
}

void CVPresenter::FillTree(Double_t value, Bool_t istemperature){
 //add a point to the humidity and temperature graphs for each sensor
 timestamp.Set(); //tell us date and time at this moment;
 timenow = timestamp.GetSec(); //number of seconds from EPOCH
 //fill temperature tree or humidity tree according to boolean
 if (istemperature){
    temp = value;
    cvtemp->Fill();
 }
 else{
    hum = value;
    cvhum->Fill();
 }
}

void CVPresenter::SetOutputFile(const char* outputfilename){
 timestamp.Set(); //tell us date and time at this moment
 TString sfilename(outputfilename); //in string format
 //outputfilename + _yearmonthday.root (NOTE: it is the start day, but it can contains also data from next days)
 TString sfilename_full = sfilename + TString(Form("_%i.root",timestamp.GetDate()));
 //opening file, saving tree
 outputfile = new TFile(sfilename_full.Data(),"NEW");
}

void CVPresenter::SaveTree() const{
 //save TTree to TFile
 outputfile->cd();
 cvtemp->SetDirectory(outputfile);
 cvtemp->AutoSave();
 cvhum->SetDirectory(outputfile);
 cvhum->AutoSave();
 outputfile->SaveSelf();
}
 
ClassImp(CVPresenter)
