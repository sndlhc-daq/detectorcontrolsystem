file(GLOB_RECURSE lib_SOURCES LIST_DIRECTORIES false *.cpp)
file(GLOB_RECURSE lib_HEADERS LIST_DIRECTORIES false *.hpp)

# everything in the aforementioned files will be compiled in one big static library
add_library(lib STATIC ${lib_SOURCES} ${lib_HEADERS})

# add the lib directory (i.e. CMAKE_CURRENT_SOURCE_DIR) to the include path
target_include_directories(lib PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_link_libraries(lib ${ROOT_LINK_TARGETS})

