#include "dcs_presenter.hpp"

DCSPresenter::DCSPresenter(){
    timestamp = TTimeStamp();
    //constructor, TTree initialization
    smstree = new TTree("smstree","TTree with Survey Monitoring System data");
    smstree->Branch("monitortime",&timenow, "monitortime/l"); //l is for long unsigned int branches
    smstree->Branch("temp",&temp,"temp[5]/D");
    smstree->Branch("relhum",&relhum,"relhum[5]/D");
    smstree->Branch("smk",&smk,"smk[3]/O");
    smstree->Branch("coolingon",&coolingon,"coolingon/O"); 
}

DCSPresenter::~DCSPresenter(){
    if (outputfile){ 
        outputfile->Close(); //close eventually still opened file
    }
}

void DCSPresenter::ClearTree(){
 //clearing all TTree entries, but preserves structures
 smstree->Reset();
}

void DCSPresenter::NextMonitoringString(std::string nextstring){
 //we prepare the parser with monitoring info.
 //Alarms are not included here, since they should be single-case anomalies
 //not something we plot vs time  
 cbparser.UpdateStatus(nextstring);
}
void DCSPresenter::NextMonitoringFile(const char* inputfilename){
 cbparser.UpdateStatusFromFile(inputfilename);
}

void DCSPresenter::FillTree(){
 //add a point to the humidity and temperature graphs for each sensor
 timestamp.Set(); //tell us date and time at this moment;
 timenow = timestamp.GetSec(); //number of seconds from EPOCH
 //loop over sensors
 for (int isensor = 0; isensor < ntempsensors; isensor++){
     temp[isensor] = cbparser.GetTemperature(isensor+1);
     relhum[isensor] = cbparser.GetHumidity(isensor+1);
 }
 for (int isensor = 0; isensor < nsmksensors; isensor++){
      smk[isensor] = cbparser.IsSmokingTriggered(isensor+1);
 }
 coolingon = cbparser.IsCoolingOn();
 smstree->Fill();
}

void DCSPresenter::SetOutputFile(const char* outputfilename){
 timestamp.Set(); //tell us date and time at this moment
 TString sfilename(outputfilename); //in string format
 //outputfilename + _yearmonthday.root (NOTE: it is the start day, but it can contains also data from next days)
 TString sfilename_full = sfilename + TString(Form("_%i.root",timestamp.GetDate()));
 //opening file, saving tree
 outputfile = new TFile(sfilename_full.Data(),"NEW");
}

void DCSPresenter::SaveTree() const{
 //save TTree to TFile
 outputfile->cd();
 smstree->SetDirectory(outputfile);
 smstree->AutoSave();
 outputfile->SaveSelf();
}
 
ClassImp(DCSPresenter)
